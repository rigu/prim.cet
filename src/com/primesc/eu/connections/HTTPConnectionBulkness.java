package com.primesc.eu.connections;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.primesc.eu.bean.bulkness.MTMessageRequest;
import com.primesc.eu.bean.bulkness.MTMessageResponse;
import com.primesc.eu.exceptions.HTTPConnectionException;
import com.primesc.eu.messages.bean.MessageBeanSMS;
import com.primesc.eu.values.PrimescEu;
import com.primesc.eu.values.SendMessages.Status;


public class HTTPConnectionBulkness extends HTTPConnectionMaster {
	private static Logger log = LoggerFactory.getLogger( HTTPConnectionBulkness.class );	
	private static final String _CONNECTION_TYPE = "bulkness";
	
	private static final HTTPConnectionBulkness _INSTANCE = new HTTPConnectionBulkness();
	
	private HTTPConnectionBulkness(){
		try {
			super.init( _CONNECTION_TYPE );
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	}
	
	public static HTTPConnectionBulkness getInstance(){
		return _INSTANCE;
	}
	
	public void sendSMSList( List<MTMessageRequest> sms )
	{
		synchronized ( this ) {
			
			this.strURL = this.strURL + properties.getProperty("bulkness.url.send.sms");
			
			int sendedLimit = maxForConnection;
			for( MTMessageRequest msg : sms ) {
				
				try {
					msg.setStatus( Status.DELIVERED_WAIT.getStatus() );
					msg.setUserAndPasswd(user, passwd);
					
					System.out.println(strURL + msg.getURLParameters(encoding));
					
					URL url = new URL(strURL + msg.getURLParameters(encoding) );
					
					if ( sendedLimit >= maxForConnection  ) {	
						reInitConnection( url );
						sendedLimit = 0;
						
					} else {										
						this.connect( url );					
					}
					
					sendedLimit++;
					
					sendSMS();
					
					msg.setStatus( Status.SENT_OK.getStatus() );
					
				} catch (IOException e) {
					msg.setStatus( Status.FAILED.getStatus() );
					log.error("IOException", e);
					
				} catch (HTTPConnectionException e) {
					msg.setStatus( Status.FAILED.getStatus() );
					log.error("HTTPConnectionException", e);
				}
			}
			
			closeConnection();
		}
	}
	
	public MTMessageResponse sendSMS( MessageBeanSMS sms ) throws IOException, HTTPConnectionException
	{
		synchronized ( this ) {		
			
			if( PrimescEu._forTest ) {
				return new MTMessageResponse();
			}
			
			this.strURL = this.strURL + properties.getProperty("bulkness.url.send.sms");
			
			int sendedLimit = maxForConnection;
			
			MTMessageResponse response = null;
			
			MTMessageRequest msg = new MTMessageRequest( sms );
			
			msg.setUserAndPasswd(user, passwd);
				
			System.out.println(strURL + msg.getURLParameters(encoding));
				
			URL url = new URL(strURL + msg.getURLParameters(encoding) );
				
			if ( sendedLimit >= maxForConnection  ) {	
				reInitConnection( url );
				sendedLimit = 0;
					
			} else {										
				this.connect( url );					
			}
				
			sendedLimit++;
				
			response = sendSMS();	
			
			closeConnection();
			
			return response;
		}
	}
	
	private MTMessageResponse sendSMS() throws IOException 
	{
		synchronized ( this ) {	
			StringBuffer resp = getResponse();	    
			return parseResponse( resp.toString() );
		}
	}
	
	private MTMessageResponse parseResponse( String resp ) 
	{
		synchronized ( this ) {	
			log.info("Resp = " + resp);
			
			Gson gson = new Gson();
			MTMessageResponse response = gson.fromJson(resp, MTMessageResponse.class);
			
			log.info( response.getReply().get(0).toString() );
	
			response.setReplyText( resp.toString() );
			
			return response;
		}
	}	
}
