/**
 * 
 */
package com.primesc.eu.connections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.primesc.eu.exceptions.HTTPConnectionException;
import com.primesc.eu.object.ObjectInitProperties;
import com.primesc.eu.values.PrimescEu;


/**
 * @author Igor Afteni
 *
 */
public abstract class HTTPConnectionMaster extends ObjectInitProperties {
	
	public static final int _CONNECTION_TIMEOUT = 500;
	
	protected String strURL;
	protected String user;
	protected String passwd;
	
	protected String encoding;
	protected String requestMethod;
	
	protected int maxForConnection;
	
	protected Map<String, String> requestProperties;
	
	protected HttpURLConnection connection;	
	
	protected void init( String type ) throws IOException {

		super.init();
		
		strURL = properties.getProperty(type + ".url", "");
		user = properties.getProperty(type + ".user", "");
		passwd = properties.getProperty(type + ".passwd", "");
		encoding = properties.getProperty(type + ".encoding", "UTF-8");
		requestMethod = properties.getProperty(type + ".requestMethod", "GET");
		
		try{
			String strTmp = properties.getProperty(type + ".max.SMSPerConnection", "5");
			maxForConnection = Integer.valueOf(strTmp).intValue();
		} catch(Exception e){}
		
		reInitRequestProperties();
		
		this.initOk = true;
	}
	
	protected void reInitRequestProperties() {
		if( requestProperties!=null ) {
			requestProperties.clear();
			
		} else {
			requestProperties = new HashMap<String, String>();
		}
		
		requestProperties.put("Cache-Control", "max-age=0");
		requestProperties.put("connection","Keep-Alive");
		requestProperties.put("connection", "close");
	}
	
	/**
	 * close and open new connection after timeout = _CONNECTION_TIMEOUT
	 * 
	 * @param URL url parameter that will provide connection, must be not null 
	 * @throws HTTPConnectionException if url is null 
	 * 
	 */
	protected void reInitConnection( URL url ) throws IOException, HTTPConnectionException{		
		closeConnection();
		if( url==null ){
			throw new HTTPConnectionException("URL cannot be null!");
		}

		connect( url );
	}
	
	protected void connect() 
			throws IOException, HTTPConnectionException 
	{
		this.connect(null);
	}
	
	protected void connect( URL url ) 
			throws IOException, HTTPConnectionException 
	{
		if( url==null && ( strURL==null || strURL.length()==0 )  ) {
			throw new HTTPConnectionException("URL cannot be null!");
			
		} else if( url==null && strURL!=null  ){
			
			url = new URL( strURL );
		}
		
		
		connection = (HttpURLConnection) url.openConnection();
		connection.setUseCaches (false);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod(requestMethod);
			
		if( requestProperties.size() > 0 ) {
			Iterator<Entry<String, String>> it = requestProperties.entrySet().iterator();
			while( it.hasNext() ) {
				Entry<String, String> entryProp = it.next();
				
				connection.setRequestProperty( entryProp.getKey() , entryProp.getValue() );
			}
		}
					
		connection.setConnectTimeout( _CONNECTION_TIMEOUT );
		connection.connect();
	} 
	
	protected StringBuffer getResponse() throws IOException {
		StringBuffer response = new StringBuffer();
		
		int respCode = connection.getResponseCode();
		
		//Get Response	
	    InputStream is = null;
	    
	    if( respCode==200 )
	    	is = connection.getInputStream();
	    else
	    	is = connection.getErrorStream();
	    
	    if( PrimescEu._forTest ){
		    System.out.println("Content Encoding: " + connection.getContentEncoding());
		    System.out.println("    Content Type: " + connection.getContentType());
		    System.out.println("Response Message: " + connection.getResponseMessage());
		    System.out.println("   Response Code: " + respCode);
	    }
	    
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is, encoding));
	    String line; 
	    while((line = rd.readLine()) != null) {
	    	response.append(line);
	    }
	    rd.close();
	    
	    return response;
	}
	
	
	
	protected StringBuffer getResponseJSON() throws IOException, HTTPConnectionException 
	{
				
		return getResponseJSON(null);
	}
	
	protected StringBuffer getResponseJSON( String strUrl ) throws IOException, HTTPConnectionException 
	{
		URL url = null;
		
		if( strUrl!=null )
			url= new URL(strUrl);
		
		reInitRequestProperties();
		requestProperties.put("Accept", "application/json");
		this.connect( url );
		
		StringBuffer response = this.getResponse();
		
		return response;
	}
	
	protected void closeConnection(){
		if( connection!=null  )
			connection.disconnect();		
	}

	
	
	/**** only for tests */
	
	public String getAllProperties(){
		StringBuffer result = new StringBuffer();

		result.append( "Connection Object = " + this.getClass().getSimpleName() + "; \n" );
		result.append( "strURL = " + strURL + "; \n");
		result.append( "user = " + user + "; \n");
		result.append( "passwd = " + passwd + "; \n");
		result.append( "encoding = " + encoding + "; \n");
		result.append( "requestMethod = " + requestMethod + "; \n");
		result.append( "maxForConnection = " + maxForConnection + "; ");
		
		return result.toString();
	}
}
