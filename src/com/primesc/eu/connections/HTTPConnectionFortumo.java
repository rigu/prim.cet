/**
 * 
 */
package com.primesc.eu.connections;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.primesc.eu.exceptions.HTTPConnectionException;

/**
 * @author Igor Afteni
 *
 */
public class HTTPConnectionFortumo extends HTTPConnectionMaster 
{
	private static Logger log = LoggerFactory.getLogger( HTTPConnectionFortumo.class );
	private static final String _CONNECTION_TYPE = "fortumo";
	
	public HTTPConnectionFortumo() {
		try {
			init( _CONNECTION_TYPE );
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error( e.getMessage() );
		}
	}

	public StringBuffer getResponseXML() throws IOException, HTTPConnectionException 
	{
		this.connect();
		return this.getResponse();		
	}
}
