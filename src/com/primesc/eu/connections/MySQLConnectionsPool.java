/**
 * 
 */
package com.primesc.eu.connections;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.primesc.eu.exceptions.MySQLCriticalException;
import com.primesc.eu.values.PrimescEu;

/**
 * @author Igor Afteni
 *
 */
public class MySQLConnectionsPool {
	private static final Logger log = LoggerFactory.getLogger(MySQLConnectionsPool.class );
	
	private static MySQLConnectionsPool _ConnectionsPOOL = new MySQLConnectionsPool();
	
	public static final int 	_MAX_POOL_CONNECTIONS = 10;	
	public static final String  _MYSQL_CLASS	= "com.mysql.jdbc.Driver";
	
	@SuppressWarnings("rawtypes")
	private GenericObjectPool connectionPool_msg = null;
	@SuppressWarnings("rawtypes")
	private GenericObjectPool connectionPool_cet = null;
	
	private DataSource dataSource_msg = null;	
	private DataSource dataSource_cet = null;

	private String _mySQLURL_cet;
	private String _user_cet;
	private String _passwd_cet;

	private String _mySQLURL_msg;
	private String _user_msg;
	private String _passwd_msg;
	
	public boolean isInitialised = false;
	
	private MySQLConnectionsPool(){
		try {
			init();		
			isInitialised = true;
			
		} catch (IOException e) {
			log.error("Constructor: IOException to init MySQLConnection pool object");
			log.error("IOException", e);
			
		} catch (InstantiationException e) {
			log.error("Constructor: InstantiationException to init MySQLConnection pool object");
			log.error("InstantiationException", e);
			
		} catch (IllegalAccessException e) {
			log.error("Constructor: IllegalAccessException to init MySQLConnection pool object");
			log.error("IllegalAccessException", e);	
					
		} catch (ClassNotFoundException e) {
			log.error("Constructor: ClassNotFoundException to init MySQLConnection pool object");
			log.error("ClassNotFoundException", e);
			
		} catch (Exception e) {
			log.error("Constructor: Exception to init MySQLConnection pool object");
			log.error("Exception", e);
		}
	}

	/**
	 * @return the ConnectionsPool
	 * @throws MySQLCriticalException 
	 */
	public static MySQLConnectionsPool getConnectionsPool() throws MySQLCriticalException 
	{	
		if( _ConnectionsPOOL==null )
			_ConnectionsPOOL = new MySQLConnectionsPool();
		
		if( !_ConnectionsPOOL.isInitialised ){
			throw new MySQLCriticalException("ERROR on MySQLConnectionsPool initialisation");
		}
		
		return _ConnectionsPOOL;	
	}
	
	@SuppressWarnings({ "rawtypes", "unused" })
	private void init() throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException 
	{		
		synchronized ( this ) {
			
			InputStream inStream = this.getClass()
					.getResourceAsStream("/com/primesc/eu/properties/" + this.getClass().getSimpleName()+".properties");
	
			Properties props = new Properties();
			props.load(inStream);
			
			
			if( PrimescEu._forTest ) {
				this._mySQLURL_cet 	= props.getProperty("mysql.url.cet.test", "");
				this._mySQLURL_msg 	= props.getProperty("mysql.url.msg.test", "");
				
				this._user_msg 		= this._user_cet 	= props.getProperty("mysql.user.test", "");
				this._passwd_msg 	= this._passwd_cet 	= props.getProperty("mysql.passwd.test", "");
				
			} else {
				this._mySQLURL_cet 	= props.getProperty("mysql.url.cet", "");
				this._mySQLURL_msg 	= props.getProperty("mysql.url.msg", "");
				
				this._user_msg 		= this._user_cet 	= props.getProperty("mysql.user.live", "");
				this._passwd_msg 	= this._passwd_cet 	= props.getProperty("mysql.passwd.live", "");
			}
						
			Class.forName(_MYSQL_CLASS).newInstance();
	
			connectionPool_msg = new GenericObjectPool();
			connectionPool_msg.setMaxActive(_MAX_POOL_CONNECTIONS);
	
			connectionPool_cet = new GenericObjectPool();
			connectionPool_cet.setMaxActive(_MAX_POOL_CONNECTIONS);
			
			//
			// Creates a connection factory object which will be use by
			// the pool to create the connection object. We passes the
			// JDBC url info, username and password.
			//
			ConnectionFactory cf_msg = new DriverManagerConnectionFactory( _mySQLURL_msg, _user_msg, _passwd_msg);
			ConnectionFactory cf_cet = new DriverManagerConnectionFactory( _mySQLURL_cet, _user_cet, _passwd_cet);
			
			//
			// Creates a PoolableConnectionFactory that will wraps the
			// connection object created by the ConnectionFactory to add
			// object pooling functionality.
			//
			PoolableConnectionFactory pcf_msg = new PoolableConnectionFactory(cf_msg, connectionPool_msg, null, null, false, false);
			PoolableConnectionFactory pcf_cet = new PoolableConnectionFactory(cf_cet, connectionPool_cet, null, null, false, false);
			
			dataSource_msg = new PoolingDataSource(connectionPool_msg);
			dataSource_cet = new PoolingDataSource(connectionPool_cet);
		}
	}
	
	/**
	 * @return the connection to _msg DB
	 * @throws SQLException 
	 */
	public Connection getConnectionMsg() throws SQLException {
		synchronized ( this ) {
			Connection conn = dataSource_msg.getConnection();		
		
			return conn;
		}
	}

	/**
	 * @return the connection to _cet DB
	 * @throws SQLException 
	 */
	public Connection getConnectionCet() throws SQLException {
		synchronized ( this ) {
			
			Connection conn = dataSource_cet.getConnection();
			
			return conn;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public GenericObjectPool getConnectionPoolMsg() {
		return connectionPool_msg;
	}
	
	@SuppressWarnings("rawtypes")
	public GenericObjectPool getConnectionPoolCet() {
		return connectionPool_cet;
	}
	
	/**
	     * Prints connection pool status.
	     */
	public void printStatus() {
		log.info("MSG Max   : " + getConnectionPoolMsg().getMaxActive() + "; " +
				 "MSG Active: " + getConnectionPoolMsg().getNumActive() + "; " +
				 "MSG Idle  : " + getConnectionPoolMsg().getNumIdle());
		
		log.info("CET Max   : " + getConnectionPoolCet().getMaxActive() + "; " +
				 "CET Active: " + getConnectionPoolCet().getNumActive() + "; " +
				 "CET Idle  : " + getConnectionPoolCet().getNumIdle() );
	}
}
