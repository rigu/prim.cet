/**
 * 
 */
package com.primesc.eu.connections;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.primesc.eu.exceptions.HTTPConnectionException;


/**
 * @author Igor Afteni
 *
 */
public class HTTPConnectionSmsCoin extends HTTPConnectionMaster {
	private static Logger log = LoggerFactory.getLogger( HTTPConnectionSmsCoin.class );	
	
	private static final String _CONNECTION_TYPE = "smscoin";
	
	public static final String _JSON_RESPONSE_PREFIX = "JSONResponse = ";
	
	private String defaultUrl;
	
	private String uriLang;
	private String uriJson;
	
	public HTTPConnectionSmsCoin() {
		try {
			init();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error( e.getMessage() );
		}
	}
	
	protected void init() throws IOException 
	{
		super.init( _CONNECTION_TYPE );
		
		uriLang = properties.getProperty("smscoin.lang");
		uriJson = properties.getProperty("smscoin.key");
		
		defaultUrl = strURL;
		
		strURL = strURL + uriJson;
	}
	
	public String getCountriesData() {
		String response = null;
		
		try {
			response = this.getResponseJSON().toString();
			
			int index = response.indexOf( _JSON_RESPONSE_PREFIX );
			
			if( index > -1 ) {
				response = response.substring( index + _JSON_RESPONSE_PREFIX.length() ).trim();
			}
			
		} catch (IOException e) {
			log.error("IOException", e);
			
		} catch (HTTPConnectionException e) {
			log.error("HTTPConnectionException", e);
			
		}
		
		return response;
	}
	
	public String getCountriesData( String lang ) {
		String response = null;
		
		try {
			strURL = defaultUrl + uriLang + lang + uriJson;
			
			response = this.getResponseJSON().toString();
			
			int index = response.indexOf( _JSON_RESPONSE_PREFIX );
			
			if( index > -1 ) {
				response = response.substring( index + _JSON_RESPONSE_PREFIX.length() ).trim();
			}
			
		} catch (IOException e) {
			log.error("IOException", e);
			
		} catch (HTTPConnectionException e) {
			log.error("HTTPConnectionException", e);
			
		}
		
		return response;
	}
	

	
	public String getCountriesData( String lang, int key ) {
		String response = null;
		
		try {
			if( key>0 ) {
				uriJson = properties.getProperty( "smscoin.key"+key );				
			}
			
			strURL = defaultUrl + uriLang + lang + uriJson;
			
			log.info(strURL);
			
			response = this.getResponseJSON().toString();
			
			int index = response.indexOf( _JSON_RESPONSE_PREFIX );
			
			if( index > -1 ) {
				response = response.substring( index + _JSON_RESPONSE_PREFIX.length() ).trim();
			}
			
		} catch (IOException e) {
			log.error("IOException", e);
			
		} catch (HTTPConnectionException e) {
			log.error("HTTPConnectionException", e);
			
		}
		
		return response;
	}
}
