/**
 * 
 */
package com.primesc.eu.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.primesc.eu.db.mysql.MySQLUtils;
import com.primesc.eu.thread.sync.SynchronizeMaster;

/**
 * @author Igor Afteni
 *
 */
public class MapDB implements Runnable {
	private static final  Logger    log 	= LoggerFactory.getLogger(MapDB.class);
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		SynchronizeMaster sync = SynchronizeMaster.getInstance();
		
		sync.setWaitForMapping();
		try{
			MySQLUtils conn = new MySQLUtils();
			
			//log.info("%%%%%%%%%%%%%%%%%%%%%% Start to map users %%%%%%%%%%%%%%%%%%%%%%");
			
			conn.mapMsgDatas();
		
			conn.finalize();
			
			//log.info("%%%%%%%%%%%%%%%%%%%%%%  end to map users  %%%%%%%%%%%%%%%%%%%%%%");
			
		} catch(Exception e) {
			log.error("", e);
			
		} catch (Throwable e) {
			log.error("", e);
			
		}
		
		sync.disposeAfterMapping();
	}

}
