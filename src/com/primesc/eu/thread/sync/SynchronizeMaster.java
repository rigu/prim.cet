package com.primesc.eu.thread.sync;

import java.util.concurrent.atomic.AtomicBoolean;

import org.jfree.util.Log;


public class SynchronizeMaster {
	private static volatile SynchronizeMaster _INSTANCE = new SynchronizeMaster();
	
	private AtomicBoolean waitForMapping;
	private AtomicBoolean working;
	
	private SynchronizeMaster(){
		waitForMapping 	= new AtomicBoolean( false );
		working 		= new AtomicBoolean( false );
	}
	
	public static SynchronizeMaster getInstance(){
		return _INSTANCE;
	}
	
	public void waitMappingToContinue() 
					throws InterruptedException
	{
		while( isWaitForMapping() ) {			
			Thread.sleep( 200 );
		}
	}

	/**
	 * @return the waitForMapping
	 */
	public boolean isWaitForMapping() {
		return waitForMapping.get();
	}
	
	/**
	 * @return the waitForMapping
	 */
	public boolean isWorking() {
		return working.get();
	}

	/**
	 * @param set working = true
	 */
	public synchronized boolean startWork() {
		System.out.println("synchronized startWork = " + working.get() );
		boolean result = !working.get();
		if( result )
			this.working.set(true);
		
		return result;
	}
	
	/**
	 * @param set working = false
	 */
	public synchronized void stopWorking() {
		this.working.set(false);
		notifyAll();
	}
	
	/**
	 * @param set waitForMapping = true
	 */
	public synchronized void setWaitForMapping() {
		this.waitForMapping.set(true);
	}
	
	/**
	 * @param set waitForMapping = false
	 */
	public synchronized void disposeAfterMapping() {
		this.waitForMapping.set(false);
		notifyAll();
	}
}
