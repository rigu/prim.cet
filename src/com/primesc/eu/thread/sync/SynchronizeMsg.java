package com.primesc.eu.thread.sync;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Igor Afteni
 *
 */
public class SynchronizeMsg {
	
	public static final int _MAX_THREAD_NR = 20;
	
	private static volatile SynchronizeMsg _INSTANCE = new SynchronizeMsg();
	
	private AtomicLong	startTime;
	private AtomicLong	endTime;
	
	private AtomicInteger generatedThreads;
	private AtomicInteger workingThreads;
	private AtomicInteger finishedThreads;
	
	private AtomicInteger registeredMsgEmail;
	private AtomicInteger registeredMsgSMS;
	
	private SynchronizeMsg(){
		init();
	}
	
	public static SynchronizeMsg getInstance(){
		return _INSTANCE;
	}
	
	public void reset() {
		if( workingThreads.get()==0 && getWaitingThreads()==0 ) {
			startTime.set( 0 );
			endTime.set( 0 );
			
			generatedThreads.set( 0 );
			workingThreads.set( 0 );
			finishedThreads.set( 0 );
			
			registeredMsgEmail.set( 0 );
			registeredMsgSMS.set( 0 );
		}
	}
	
	private void init() {	
		startTime	= new AtomicLong();
		endTime		= new AtomicLong();
		
		generatedThreads	= new AtomicInteger();
		workingThreads		= new AtomicInteger();
		finishedThreads		= new AtomicInteger();
		
		registeredMsgEmail 	= new AtomicInteger();
		registeredMsgSMS	= new AtomicInteger();
	}
	
	public void setStartTime(){
		startTime.set( System.currentTimeMillis() );
	}

	public void setEndTime(){
		endTime.set( System.currentTimeMillis() );
	}
	
	public int getEndTime(){
		setEndTime();
		return getTime();
	}
	
	public int getTime(){		
		return Long.valueOf( (endTime.get() - startTime.get())/1000 ).intValue();
	}

	public void addGeneratedThread() {
		generatedThreads.incrementAndGet();
	}
	
	public void startToWork() {
		workingThreads.incrementAndGet();
	}
	
	public synchronized void finishToWork() {
		workingThreads.decrementAndGet();
		finishedThreads.incrementAndGet();
	}
	
	public int getGeneratedThreads() {
		return generatedThreads.get();
	}
	
	public int getWorkingThreads() {
		return workingThreads.get();
	}
	
	public int getFinishedThreads() {
		return finishedThreads.get();
	}
	
	public synchronized int getWaitingThreads() {
		return generatedThreads.get() - finishedThreads.get() - workingThreads.get();
	}
	
	
	/**
	 * @return the registeredMsgTotal
	 */
	public synchronized int getRegisteredMsgTotal() {		
		return registeredMsgEmail.get() + registeredMsgSMS.get();
	}

	/**
	 * @return the registeredMsgEmail
	 */
	public synchronized int getRegisteredMsgEmail() {
		return registeredMsgEmail.get();
	}

	/**
	 * @return the registeredMsgSMS
	 */
	public synchronized int getRegisteredMsgSMS() {
		return registeredMsgSMS.get();
	}	

	/**
	 * @param registeredMsgEmail the registeredMsgEmail to set
	 */
	public synchronized void addRegisteredMsgEmail( ) {
		registeredMsgEmail.incrementAndGet();
	}

	/**
	 * @param registeredMsgSMS the registeredMsgSMS to set
	 */
	public synchronized void addRegisteredMsgSMS() {
		registeredMsgSMS.incrementAndGet();
	}
	
	
}
