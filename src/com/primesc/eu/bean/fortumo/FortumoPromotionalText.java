/**
 * 
 */
package com.primesc.eu.bean.fortumo;

/**
 * @author Igor Afteni
 *
 */
public class FortumoPromotionalText {
	private String lang;
	private String text;
	
	public String getAsString() {
		return "lang = " + lang + "; "
			 + "text = " + text ;
	}
	
	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}
	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
}
