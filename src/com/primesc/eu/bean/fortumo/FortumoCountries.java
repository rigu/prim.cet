/**
 * 
 */
package com.primesc.eu.bean.fortumo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author x01grobog
 *
 */
public class FortumoCountries {
	private int id;
	private String checksum;
	
	private String statusCode;
	private String statusMessage;
	
	private String serviceId;
	
	private List<FortumoCountry> countries = new ArrayList<FortumoCountry>();
	
	public void addCountry( FortumoCountry country ) {
		countries.add(country);
	}
	
	public String getAsString() {
		return "statusCode = " + statusCode + "; "
			 + "statusMessage = " + statusMessage + "; "
			 + "serviceId = " + serviceId;
	}
	
	
	
	/**
	 * @return the checksum
	 */
	public String getChecksum() {
		return checksum;
	}

	/**
	 * @param checksum the checksum to set
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the countries
	 */
	public List<FortumoCountry> getCountries() {
		return countries;
	}

	/**
	 * @param countries the countries to set
	 */
	public void setCountries(List<FortumoCountry> countries) {
		this.countries = countries;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}
	/**
	 * @param statusMessage the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
}
