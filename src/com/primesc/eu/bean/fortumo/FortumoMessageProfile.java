/**
 * 
 */
package com.primesc.eu.bean.fortumo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Afteni
 *
 */
public class FortumoMessageProfile {
	private int id;
	
	private String all_operators;
	private String double_confirmation_shortcode;
	private String double_confirmation_text;	
	private String shortcode;
	private String keyword;
	
	private List<FortumoOperator> operators = new ArrayList<FortumoOperator>();

	public void addOperator( FortumoOperator operator ) {
		operators.add(operator);
	}
	
	public String getAsString() {
		return "shortcode = " + shortcode + "; "
			 + "keyword = " + keyword + "; "
			 + "double_confirmation_shortcode = " + double_confirmation_shortcode + "; "
			 + "double_confirmation_shortcode = " + double_confirmation_shortcode + "; "
			 + "all_operators = " + all_operators;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the operators
	 */
	public List<FortumoOperator> getOperators() {
		return operators;
	}

	/**
	 * @param operators the operators to set
	 */
	public void setOperators(List<FortumoOperator> operators) {
		this.operators = operators;
	}

	/**
	 * @return the all_operators
	 */
	public String getAll_operators() {
		return all_operators;
	}

	/**
	 * @param all_operators the all_operators to set
	 */
	public void setAll_operators(String all_operators) {
		this.all_operators = all_operators;
	}

	/**
	 * @return the shortcode
	 */
	public String getShortcode() {
		return shortcode;
	}

	/**
	 * @param shortcode the shortcode to set
	 */
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * @return the operators
	 */
	public List<FortumoOperator> getCountries() {
		return operators;
	}

	/**
	 * @param operator the operators to set
	 */
	public void setCountries(List<FortumoOperator> operator) {
		this.operators = operator;
	}

	/**
	 * @return the double_confirmation_shortcode
	 */
	public String getDouble_confirmation_shortcode() {
		return double_confirmation_shortcode;
	}

	/**
	 * @param double_confirmation_shortcode the double_confirmation_shortcode to set
	 */
	public void setDouble_confirmation_shortcode(
			String double_confirmation_shortcode) {
		this.double_confirmation_shortcode = double_confirmation_shortcode;
	}

	/**
	 * @return the double_confirmation_text
	 */
	public String getDouble_confirmation_text() {
		return double_confirmation_text;
	}

	/**
	 * @param double_confirmation_text the double_confirmation_text to set
	 */
	public void setDouble_confirmation_text(String double_confirmation_text) {
		this.double_confirmation_text = double_confirmation_text;
	}
	
}
