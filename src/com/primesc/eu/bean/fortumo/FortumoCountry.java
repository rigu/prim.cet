package com.primesc.eu.bean.fortumo;

import java.util.ArrayList;
import java.util.List;

public class FortumoCountry {
	private int id;
	
	private String code;
	private String approved;
	private String vat;
	private String name;
	
	private List<FortumoPrice> priceList = new ArrayList<FortumoPrice>();
	private List<FortumoPromotionalText> promoList = new ArrayList<FortumoPromotionalText>();

	public void addPrice( FortumoPrice price ) {
		priceList.add( price );
	}

	public void addPromo( FortumoPromotionalText promo ) {
		promoList.add( promo );
	}
	
	public String getAsString() {
		return "code = " + code + "; "
			 + "approved = " + approved + "; "
			 + "vat = " + vat + "; "
			 + "name = " + name;
	}
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the promoList
	 */
	public List<FortumoPromotionalText> getPromoList() {
		return promoList;
	}

	/**
	 * @param promoList the promoList to set
	 */
	public void setPromoList(List<FortumoPromotionalText> promoList) {
		this.promoList = promoList;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the approved
	 */
	public String getApproved() {
		return approved;
	}

	/**
	 * @param approved the approved to set
	 */
	public void setApproved(String approved) {
		this.approved = approved;
	}

	/**
	 * @return the vat
	 */
	public String getVat() {
		return vat;
	}

	/**
	 * @param vat the vat to set
	 */
	public void setVat(String vat) {
		this.vat = vat;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the priceList
	 */
	public List<FortumoPrice> getPriceList() {
		return priceList;
	}

	/**
	 * @param priceList the priceList to set
	 */
	public void setPriceList(List<FortumoPrice> priceList) {
		this.priceList = priceList;
	}

}
