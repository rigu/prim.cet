/**
 * 
 */
package com.primesc.eu.bean.fortumo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Afteni
 *
 */
public class FortumoPrice {
	private int id;
	
	private String vat_included;
	private String all_operators;
	private String currency;
	private String amount;
	
	private List<FortumoMessageProfile> msgProfile = new ArrayList<FortumoMessageProfile>();

	public void addMessageProfile( FortumoMessageProfile msg ) {
		msgProfile.add( msg );
	}
	
	public String getAsString() {
		return "vat_included = " + vat_included + "; "
			 + "all_operators = " + all_operators + "; "
			 + "currency = " + currency + "; "
			 + "amount = " + amount;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the vat_included
	 */
	public String getVat_included() {
		return vat_included;
	}

	/**
	 * @param vat_included the vat_included to set
	 */
	public void setVat_included(String vat_included) {
		this.vat_included = vat_included;
	}

	/**
	 * @return the all_operators
	 */
	public String getAll_operators() {
		return all_operators;
	}

	/**
	 * @param all_operators the all_operators to set
	 */
	public void setAll_operators(String all_operators) {
		this.all_operators = all_operators;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the msgProfile
	 */
	public List<FortumoMessageProfile> getMsgProfile() {
		return msgProfile;
	}

	/**
	 * @param msgProfile the msgProfile to set
	 */
	public void setMsgProfile(List<FortumoMessageProfile> msgProfile) {
		this.msgProfile = msgProfile;
	}
	
}
