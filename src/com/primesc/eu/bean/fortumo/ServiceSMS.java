/**
 * 
 */
package com.primesc.eu.bean.fortumo;

import javax.servlet.http.HttpServletRequest;

/**
 * @author x01grobog
 *
 */
public class ServiceSMS {

	private String message;
	private String sender;
	private String country;
	private String price;
	private String currency;
	private String service_id;
	private String message_id;
	private String keyword;
	private String shortcode;
	private String operator;
	private String billing_type;
	private String status;
	private String test;
	private String sig;
	
	private String strResponse;
	private String regCode;
	
	public ServiceSMS(){}
	
	public ServiceSMS( HttpServletRequest request ){
		this.message 		= request.getParameter("message");
		this.sender 		= request.getParameter("sender");
		this.country 		= request.getParameter("country");
		this.price 			= request.getParameter("price");
		this.currency 		= request.getParameter("currency");
		this.service_id		= request.getParameter("service_id");
		this.message_id 	= request.getParameter("message_id");
		this.keyword 		= request.getParameter("keyword");
		this.shortcode 		= request.getParameter("shortcode");
		this.operator 		= request.getParameter("operator");
		this.billing_type 	= request.getParameter("billing_type");
		this.status 		= request.getParameter("status");
		this.test 			= request.getParameter("test");
		this.sig 			= request.getParameter("sig");
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}
	/**
	 * @param sender the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the service_id
	 */
	public String getService_id() {
		return service_id;
	}
	/**
	 * @param service_id the service_id to set
	 */
	public void setService_id(String service_id) {
		this.service_id = service_id;
	}
	/**
	 * @return the message_id
	 */
	public String getMessage_id() {
		return message_id;
	}
	/**
	 * @param message_id the message_id to set
	 */
	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}
	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}
	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	/**
	 * @return the shortcode
	 */
	public String getShortcode() {
		return shortcode;
	}
	/**
	 * @param shortcode the shortcode to set
	 */
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}
	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}
	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}
	/**
	 * @return the billing_type
	 */
	public String getBilling_type() {
		return billing_type;
	}
	/**
	 * @param billing_type the billing_type to set
	 */
	public void setBilling_type(String billing_type) {
		this.billing_type = billing_type;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the test
	 */
	public String getTest() {
		return test;
	}
	/**
	 * @param test the test to set
	 */
	public void setTest(String test) {
		this.test = test;
	}
	/**
	 * @return the sig
	 */
	public String getSig() {
		return sig;
	}
	/**
	 * @param sig the sig to set
	 */
	public void setSig(String sig) {
		this.sig = sig;
	}

	/**
	 * @return the strResponse
	 */
	public String getStrResponse() {
		return strResponse;
	}

	/**
	 * @param strResponse the strResponse to set
	 */
	public void setStrResponse(String strResponse) {
		this.strResponse = strResponse;
	}

	/**
	 * @return the regCode
	 */
	public String getRegCode() {
		return regCode;
	}

	/**
	 * @param regCode the regCode to set
	 */
	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}
	
}
