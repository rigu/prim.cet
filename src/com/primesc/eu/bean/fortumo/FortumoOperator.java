/**
 * 
 */
package com.primesc.eu.bean.fortumo;

/**
 * @author x01grobog
 *
 */
public class FortumoOperator {
	private String code;
	private String default_billing_status;
	private String billing_type;
	private String revenue;
	private String name;
	
	public String getAsString() {
		return "code = " + code + "; "
			 + "default_billing_status = " + default_billing_status + "; "
			 + "revenue = " + revenue + "; "
			 + "name = " + name ;
	}
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the default_billing_status
	 */
	public String getDefault_billing_status() {
		return default_billing_status;
	}
	/**
	 * @param default_billing_status the default_billing_status to set
	 */
	public void setDefault_billing_status(String default_billing_status) {
		this.default_billing_status = default_billing_status;
	}
	/**
	 * @return the billing_type
	 */
	public String getBilling_type() {
		return billing_type;
	}
	/**
	 * @param billing_type the billing_type to set
	 */
	public void setBilling_type(String billing_type) {
		this.billing_type = billing_type;
	}
	/**
	 * @return the revenue
	 */
	public String getRevenue() {
		return revenue;
	}
	/**
	 * @param revenue the revenue to set
	 */
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
