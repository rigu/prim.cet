package com.primesc.eu.bean.bulkness;

import java.util.ArrayList;
import java.util.List;


public class MTMessageResponse {
	
	private List<Reply> 	reply;
	private String 			balance;
	private String			replyText;
	
	
	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}



	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	
	/**
	 * @return the replyText
	 */
	public String getReplyText() {
		return replyText;
	}



	/**
	 * @param replyText the replyText to set
	 */
	public void setReplyText(String replyText) {
		this.replyText = replyText;
	}



	/**
	 * @return the reply
	 */
	public List<Reply> getReply() {
		return reply;
	}



	/**
	 * @param reply the reply to set
	 */
	public void setReply(List<Reply> reply) {
		this.reply = reply;
	}

	
	public String getResponseString() {
		StringBuffer response = new StringBuffer();
		response.append("\n\n");
		
		if( reply==null )
			reply = new ArrayList<Reply>();
		
		for(Reply r: reply){
			response.append("reply: " + r.toString() );		
		}
		
		response.append("\n\n");
		response.append("balance: " + balance);
		response.append("\n\n");
		
		return response.toString();  
	}


	public static class Reply {
	
		private String 	status;
		private String 	number;
		private String  country;
		private String 	cost;
		private String	message_id;
		
		private int		parts;
	
		public String toString() {
			return "\n\t"
				 + "status: \t" + status + ";\n\t"
				 + "number: \t" + number + ";\n\t"
				 + "country:\t" + country + ";\n\t"
				 + "cost: \t\t" + cost + ";\n\t"
				 + "message_id: \t" + message_id + ";\n\t"
				 + "parts: \t\t" + parts + ";\n\n";
		}
	
		/**
		 * @return the status
		 */
		public String getStatus() {
			return status;
		}
	
		/**
		 * @param status the status to set
		 */
		public void setStatus(String status) {
			this.status = status;
		}
	
		/**
		 * @return the number
		 */
		public String getNumber() {
			return number;
		}
	
		/**
		 * @param number the number to set
		 */
		public void setNumber(String number) {
			this.number = number;
		}
	
		/**
		 * @return the cost
		 */
		public String getCost() {
			return cost;
		}
	
		/**
		 * @param cost the cost to set
		 */
		public void setCost(String cost) {
			this.cost = cost;
		}
	
		/**
		 * @return the message_id
		 */
		public String getMessage_id() {
			return message_id;
		}
	
		/**
		 * @param message_id the message_id to set
		 */
		public void setMessage_id(String message_id) {
			this.message_id = message_id;
		}
	
		/**
		 * @return the parts
		 */
		public int getParts() {
			return parts;
		}
	
		/**
		 * @param parts the parts to set
		 */
		public void setParts(int parts) {
			this.parts = parts;
		}

		/**
		 * @return the country
		 */
		public String getCountry() {
			return country;
		}

		/**
		 * @param country the country to set
		 */
		public void setCountry(String country) {
			this.country = country;
		}
		
		
	}
	
}
