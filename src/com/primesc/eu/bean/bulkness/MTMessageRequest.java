/**
 * 
 */
package com.primesc.eu.bean.bulkness;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.primesc.eu.messages.bean.MessageBeanSMS;

/**
 * @author Igor Afteni
 *
 */
public class MTMessageRequest extends BulknessBalance {

    private String message; 	// - required, urlencoded message body. Should not be longer than 3 sms can contain.
    private String priority; 	// - optional, can take values from 0 (lowest) to 3 (highest). default is 0.
    private String from; 		// - required, a sender addr that a message appears to come from.
    private String to; 			// - required, a destination address. 
    							//   Several destinations can be provided, separated by comma (",").  
    
    private int status=0;
	
    public MTMessageRequest(String username, String password){
    	super(username, password);
    }    
    
    public MTMessageRequest(String username, String password, String from, String to, String message) {
		super(username, password);
		this.from = from;
		this.to = to;
		this.message = message;
	}
    
    public MTMessageRequest( MessageBeanSMS sms ) {
    	super(null, null);
    	this.from 	= sms.getFrom();
    	this.to		= sms.getTo();
    	this.message	= sms.getContent();
    	this.priority	= sms.getPriority();
    	this.status		= sms.getStatus();
    }

    public String getURLParameters(String encoding) throws UnsupportedEncodingException {
    	return "?username=" + URLEncoder.encode(username, encoding)
    		 + "&password=" + URLEncoder.encode(password, encoding)
    		 + "&from="		+ URLEncoder.encode(from, encoding)
    		 + "&to="		+ URLEncoder.encode(to, encoding)
    		 + "&message="	+ URLEncoder.encode(message, encoding);
    }

	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
