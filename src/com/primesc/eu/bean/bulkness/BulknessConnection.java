package com.primesc.eu.bean.bulkness;

import java.net.MalformedURLException;
import java.net.URL;

public class BulknessConnection {
	
	public static final String 	_BULKNESS_SERVER 	= "http://api.bulkness.com";
	
	public static final String	_URI_SEND_MTM		= "/message/send/?";
	public static final String	_URI_BALANCE		= "/balance/?";
	
	
	public static URL getMTMessageURL( String params ) 
			throws MalformedURLException 
	{
		return new URL( _BULKNESS_SERVER + _URI_SEND_MTM + params );
	}
	
	public static URL getBalanceURL( String params ) 
			throws MalformedURLException 
	{
		return new URL( _BULKNESS_SERVER + _URI_BALANCE + params );
	}
	
}
