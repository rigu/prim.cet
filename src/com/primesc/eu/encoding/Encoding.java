/**
 * 
 */
package com.primesc.eu.encoding;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Afteni
 *
 */
public class Encoding {	
	private static Logger    log = LoggerFactory.getLogger(Encoding.class);
	
	public static String replaceEncodedChars(String str) throws Throwable{
		Map<String, String>	charMap = initMap();
		
		int posC = str.indexOf("%C");
		int posF = str.indexOf("%3F");
		int posS = str.indexOf("%2C");
		if( posC > -1 ){
			String to_replace = str.substring(posC, posC+6);		
			
			try{
				str = str.replaceAll(to_replace, charMap.get(to_replace));
			
			} catch(Exception e) {			
				log.error(str + "\n To replace = " + to_replace);
				log.error("", e);	
				
				throw new Exception(str + "\n To replace = " + to_replace);
			}
			
		} else if( posF > -1 ) {
			posC = str.indexOf("%3F");
			String to_replace = str.substring(posC, posC+3);
			str = str.replaceAll(to_replace, charMap.get(to_replace));
		} else if( posS > -1 ) {
			posC = str.indexOf("%2C");
			String to_replace = str.substring(posC, posC+3);
			str = str.replaceAll(to_replace, charMap.get(to_replace));
		}
		
		if( str.contains("%C") || str.contains("%3F") || str.contains("%2C"))
			str = replaceEncodedChars(str);
		
		return str;
	}
	
	
	private static Map<String, String> initMap(){
		Map<String, String>	charMap = new HashMap<String, String>();
		charMap.put("%C4%82", "A");
		charMap.put("%C4%83", "a");
		charMap.put("%C3%82", "A");
		charMap.put("%C3%A2", "a");
		charMap.put("%C3%83", "A");
		
		charMap.put("%C3%8E", "I");
		charMap.put("%C3%AE", "i");
		
		charMap.put("%C5%9E", "S");
		charMap.put("%C8%98", "S");
		charMap.put("%C5%9F", "s");

		charMap.put("%C3%9E", "T");
		charMap.put("%C5%A2", "T");
		charMap.put("%C5%A3", "t");		
		charMap.put("%C8%9A", "T");
		
		charMap.put("%3F",    "?");
		charMap.put("%2C",    ",");
		charMap.put("%C2%A0", " ");
		
		return charMap;
	}
}
