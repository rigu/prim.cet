/**
 * 
 */
package com.primesc.eu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.primesc.eu.security.SecurityUtils;
import com.primesc.eu.values.PrimescEu.SecureStart;
import com.primesc.eu.values.PrimescEu.Value;

/**
 * @author Igor Afteni
 *
 */
public class SenderMainOptions {
	public Map<Value, String>	argsKeyVal	= new HashMap<Value, String>();
	public List<Value> 			listKeys 	= new ArrayList<Value>();

	public Value getOptions( String[] args ) {
		Value toReturn = Value.NOT_START ;
		
		for( int i=0; i<args.length; i++ ) {
			String strArg = args[i];
			
			Value key = Value.NOTKEY;
			try{
				key = Value.valueOf( strArg.toUpperCase() );
				listKeys.add(key);
				
			} catch(Exception e) {
				
				if( listKeys.size()>0 ){	
					Value verifKey = listKeys.get( listKeys.size()-1 ); 
					if( !argsKeyVal.containsKey(verifKey) ){
						key = verifKey;						
					}
				}
				
				argsKeyVal.put( key, strArg );
			}
		}
		
		if( keyMatch( argsKeyVal.get( Value.STARTKEY ) ) ) {
			toReturn = Value.START ;
			
		} else {
			toReturn = Value.NOTKEY;
		}
		
		return toReturn;
	}
	
	protected boolean keyMatch( String key) {
		SecurityUtils security = 
				SecurityUtils.getInstance( SecureStart.MAILSENDER_KEY );
		
		return security.mathToStart(key);
	}
	
	public Value getMailOptions( String[] args ) {	
		
		Value toReturn = getOptions( args );
				
		if( toReturn == Value.NOTKEY ) {
			return toReturn;
		}
		
		boolean sendMail = false;
		boolean sendTo	 = false;
		boolean all		 = false;
		
		for(Value key : listKeys ){
			switch (key) {				
				case ALL: 		
						all = true;
						break;
					
				case SEND_TO :
						sendTo = true;
						break;
						
				case SENDMAIL :	
						sendMail = true;
						break;
				default:						
						break;
			}
		}
		
		if( sendMail && all ) {
			toReturn = Value.SEND_TO_ALL;
			
		} else if( sendTo && argsKeyVal.containsKey( Value.SEND_TO ) ) {
			toReturn = Value.SEND_TO;
		}
		
		
		return toReturn;
	}
}
