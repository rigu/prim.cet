/**
 * 
 */
package com.primesc.eu.json.smscoin;

import java.util.Comparator;

/**
 * @author Igor Afteni
 *
 */
public class SmsCoinProvider 
			implements Comparator<SmsCoinProvider>, Comparable<SmsCoinProvider> 
{
	private String code;
	private String name;
	private String number;
	private String prefix;
	private String rewrite;
	private double price;
	private double usd;
	private double profit;
	private double vat;
	private String currency;
	private String special;
	
	private int countryId;
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @return the number
	 */
	public int getNumberInt() {
		int result = -1;
		
		try{
			result = Integer.valueOf( number );
		} catch (Exception e){}
		
		return result;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}
	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	/**
	 * @return the rewrite
	 */
	public String getRewrite() {
		return rewrite;
	}
	/**
	 * @param rewrite the rewrite to set
	 */
	public void setRewrite(String rewrite) {
		this.rewrite = rewrite;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the usd
	 */
	public double getUsd() {
		return usd;
	}
	/**
	 * @param usd the usd to set
	 */
	public void setUsd(double usd) {
		this.usd = usd;
	}
	/**
	 * @return the profit
	 */
	public double getProfit() {
		return profit;
	}
	/**
	 * @param profit the profit to set
	 */
	public void setProfit(double profit) {
		this.profit = profit;
	}
	/**
	 * @return the vat
	 */
	public double getVat() {
		return vat;
	}
	/**
	 * @param vat the vat to set
	 */
	public void setVat(double vat) {
		this.vat = vat;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the special
	 */
	public String getSpecial() {
		return special;
	}
	/**
	 * @param special the special to set
	 */
	public void setSpecial(String special) {
		this.special = special;
	}
	/**
	 * @return the countryId
	 */
	public synchronized int getCountryId() {
		return countryId;
	}
	/**
	 * @param countryId the countryId to set
	 */
	public synchronized void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	
	public boolean equals( SmsCoinProvider provider ){
		return compareTo(provider) ==0;
	}
	
	@Override
	public int compareTo(SmsCoinProvider o) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int compare(SmsCoinProvider o1, SmsCoinProvider o2) {
		// TODO Auto-generated method stub
		return 0;
	}	
}
