package com.primesc.eu.json.smscoin;

import java.util.List;

public class SmsCoinCountry {
	
	private int id;
	private String language;
	private String lang2;
	
	private String country 		;
	private String country_name ;
	private String number		;
	private String prefix 		;
	private String rewrite 		;
	private double price 		;
	private double usd 			;
	private double profit 		;
	private double vat 			;
	private String currency 	;
	private String special 		;
	
	private String checksum		;

	private List<SmsCoinProvider> providers;

	
	public boolean haveProviders() {
		return ( providers!=null && providers.size() > 0 );
	}	
	
	/**
	 * @return the id
	 */
	public synchronized int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public synchronized void setId(int id) {
		this.id = id;
		if( haveProviders() ) {
			for( SmsCoinProvider provider : providers ) {
				provider.setCountryId(id);
			}
		}
	}
	

	/**
	 * @return the language
	 */
	public synchronized String getLanguage() {
		return language;
	}


	/**
	 * @param language the language to set
	 */
	public synchronized void setLanguage(String language) {
		this.language = language;
	}


	public String getLang2() {
		return lang2;
	}


	public void setLang2(String lang2) {
		this.lang2 = lang2;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the country_name
	 */
	public String getCountry_name() {
		return country_name;
	}

	/**
	 * @param country_name the country_name to set
	 */
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}


	/**
	 * @return the number as integer
	 */
	public int getNumberInt() {
		int nb = -1;
		
		try{
			nb = Integer.valueOf(number).intValue();
		} catch(Exception e){}
		
		return nb;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = String.valueOf(number);
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return the rewrite
	 */
	public String getRewrite() {
		return rewrite;
	}

	/**
	 * @param rewrite the rewrite to set
	 */
	public void setRewrite(String rewrite) {
		this.rewrite = rewrite;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the usd
	 */
	public double getUsd() {
		return usd;
	}

	/**
	 * @param usd the usd to set
	 */
	public void setUsd(double usd) {
		this.usd = usd;
	}

	/**
	 * @return the profit
	 */
	public double getProfit() {
		return profit;
	}

	/**
	 * @param profit the profit to set
	 */
	public void setProfit(double profit) {
		this.profit = profit;
	}

	/**
	 * @return the vat
	 */
	public double getVat() {
		return vat;
	}

	/**
	 * @param vat the vat to set
	 */
	public void setVat(double vat) {
		this.vat = vat;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the special
	 */
	public String getSpecial() {
		return special;
	}

	/**
	 * @param special the special to set
	 */
	public void setSpecial(String special) {
		this.special = special;
	}

	/**
	 * @return the providers
	 */
	public List<SmsCoinProvider> getProviders() {
		return providers;
	}

	/**
	 * @param providers the providers to set
	 */
	public void setProviders(List<SmsCoinProvider> providers) {
		this.providers = providers;
	}


	public String getChecksum() {
		return checksum;
	}


	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
}
