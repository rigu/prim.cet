/**
 * 
 */
package com.primesc.eu.object;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;

/**
 * @author Igor Afteni
 *
 */
public abstract class ObjectInitProperties {
	
	protected String strThreadId;	
	protected String propertiesType;
	
	protected boolean initOk = false;
	
	protected Properties properties;
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		strThreadId = null;
		propertiesType = null;
		
		properties.clear();
		properties = null;
		
		super.finalize();
	}

	protected void init( String type ) throws IOException 
	{
		init();		
		propertiesType = type;
	}
	
	protected void init( ) throws IOException {
		InputStream inStream = this.getClass()
				.getResourceAsStream("/com/primesc/eu/properties/" + this.getClass().getSimpleName()+".properties");

		properties = new Properties();
		properties.load(inStream);
		
		strThreadId = getThreadId();
	}
	
	protected void initOk() throws IOException
	{
		init();
		initOk = true;
	}
	
	protected void initOk( String type ) throws IOException
	{
		init( type );
		initOk = true;
	}
	
	protected String getThreadId() {
		synchronized (this) {
			long threadId = Thread.currentThread().getId();
			
			Random random = new Random(12345678);
			long generated = random.nextLong();
        	
        	int hash = (37*23) + (int)( generated ^ ( generated >>> 32) );        	
        	final String id = String.valueOf(threadId) + String.valueOf(hash).toString().replaceAll("-", "");
			
        	return id;
		}
	}

	/**
	 * @return the strThreadId
	 */
	public synchronized String getStrThreadId() {
		return strThreadId;
	}

	/**
	 * @param strThreadId the strThreadId to set
	 */
	public synchronized void setStrThreadId(String strThreadId) {
		this.strThreadId = strThreadId;
	}

	/**
	 * @return the propertiesType
	 */
	public synchronized String getPropertiesType() {
		return propertiesType;
	}

	/**
	 * @param propertiesType the propertiesType to set
	 */
	public synchronized void setPropertiesType(String propertiesType) {
		this.propertiesType = propertiesType;
	}

	/**
	 * @return the properties
	 */
	public synchronized Properties getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public synchronized void setProperties(Properties properties) {
		this.properties = properties;
	}
}
