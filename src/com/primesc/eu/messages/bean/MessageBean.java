package com.primesc.eu.messages.bean;

public class MessageBean {
	
	protected int		id;
	protected String	msg_id;
	protected int		status;
	protected int		service_id;
	
	protected String	from;
	protected String	to;
	protected String	country;
	protected String	content;
	protected String 	priority;
	
	protected double	cost;
	protected String	reply;
	
	public MessageBean() {
		// TODO Auto-generated constructor stub
	}	
	
	public MessageBean(int id) {
		this.id = id;
	}

	public void setNewBean( MessageBean newBean ){
		this.setId		( newBean.getId() 		);
		this.setMsg_id	( newBean.getMsg_id()	);
		this.setStatus	( newBean.getStatus()	);
		
		this.setService_id( newBean.getService_id() );
		
		this.setFrom( 	newBean.getFrom()	);
		this.setTo	( 	newBean.getTo() 	);
		
		this.setCountry	(	newBean.getCountry()	);		
		this.setContent	( 	newBean.getContent()	);
		this.setPriority( 	newBean.getPriority() 	);
		
		this.setCost	( 	newBean.getCost()	);
		this.setReply	( 	newBean.getReply() 	);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the msg_id
	 */
	public String getMsg_id() {
		return msg_id;
	}


	/**
	 * @param msg_id the msg_id to set
	 */
	public void setMsg_id(String msg_id) {
		this.msg_id = msg_id;
	}


	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the service_id
	 */
	public int getService_id() {
		return service_id;
	}
	/**
	 * @param service_id the service_id to set
	 */
	public void setService_id(int service_id) {
		this.service_id = service_id;
	}
	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}
	
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}


	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}


	/**
	 * @return the content
	 */
	public byte[] getContentBytes() {
		return content.getBytes();
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}


	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}


	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}
	/**
	 * @param cost the cost to set
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}
	/**
	 * @return the reply
	 */
	public String getReply() {
		return reply;
	}
	/**
	 * @param reply the reply to set
	 */
	public void setReply(String reply) {
		this.reply = reply;
	}
	
}
