package com.primesc.eu.messages.bean;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.DecoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.primesc.eu.security.SecurityUtils;

public class MessageBeanSMTP {
	private static Logger log = LoggerFactory.getLogger(MessageBeanSMTP.class);	
	
	private int id;
	private int count_last_hour;
	
	private String host;
	private String port;
	private String user;
	private String decryptedUser;
	private String passwd;
	private String decryptedPasswd;
	
	private String key;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the count_last_hour
	 */
	public int getCount_last_hour() {
		return count_last_hour;
	}
	/**
	 * @param count_last_hour the count_last_hour to set
	 */
	public void setCount_last_hour(int count_last_hour) {
		this.count_last_hour = count_last_hour;
	}
	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}
	
	public int getIntegerPort(){
		return Integer.valueOf( getPort() ).intValue();
	}
	
	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @return the user encrypted
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
	public String getEncryptedUser() 
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, 
				   IllegalBlockSizeException, BadPaddingException 
	{
		SecurityUtils sUtil = SecurityUtils.getInstance( getKey() );
		return sUtil.encrypt(user);
	}
	
	/**
	 * @return user decrypted
	 * */
	public String getDecryptedUser()  
	{
		if( null==decryptedUser && null!=this.key ) {
			try {
				SecurityUtils sUtil = SecurityUtils.getInstance( this.key );
				decryptedUser = sUtil.decrypt(user).trim();
				
			} catch (InvalidKeyException e) {
				log.error("", e);
				
			} catch (NoSuchAlgorithmException e) {
				log.error("", e);
				
			} catch (NoSuchPaddingException e) {
				log.error("", e);
				
			} catch (UnsupportedEncodingException e) {
				log.error("", e);
				
			} catch (IllegalBlockSizeException e) {
				log.error("", e);
				
			} catch (BadPaddingException e) {
				log.error("", e);
				
			} catch (DecoderException e) {
				log.error("", e);
				
			}
		}
		return decryptedUser;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the passwd
	 */
	public String getPasswd() {
		return passwd;
	}
	/**
	 * @return the passwd encrypted
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
	public String getEncryptedPasswd() 
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, 
			   IllegalBlockSizeException, BadPaddingException 
	{
		SecurityUtils sUtil = SecurityUtils.getInstance( getKey() );
		return sUtil.encrypt(passwd);
	}
	
	/**
	 * @return decryptedPasswd 
	 * */
	public String getDecryptedPasswd() 
	{
		if( null==decryptedPasswd && null!=this.key ) {
			try {
				SecurityUtils sUtil = SecurityUtils.getInstance( this.key );
				decryptedPasswd = sUtil.decrypt(passwd).trim();
				
			} catch (InvalidKeyException e) {
				log.error("", e);
				
			} catch (NoSuchAlgorithmException e) {
				log.error("", e);
				
			} catch (NoSuchPaddingException e) {
				log.error("", e);
				
			} catch (UnsupportedEncodingException e) {
				log.error("", e);
				
			} catch (IllegalBlockSizeException e) {
				log.error("", e);
				
			} catch (BadPaddingException e) {
				log.error("", e);
				
			} catch (DecoderException e) {
				log.error("", e);
				
			}
		}
		return decryptedPasswd;
	}
	/**
	 * @param passwd the passwd to set
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		if( null==key ){
			try {
				key = SecurityUtils.getInstance().generateSecretKey();
				
			} catch (NoSuchAlgorithmException e) {
				log.error("", e);
			}
		}
		
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();

		try {
			result.append("\n\nid = " + id );
			result.append("\nhost = " + host );
			result.append("\nport = " + port );
			result.append("\nkey = " + key );
			result.append("\ncount_last_hour = " + count_last_hour );
			result.append("\nuser = " + user );
			result.append("\ndecrypted user = " + getDecryptedUser() );
			result.append("\npasswd = " + passwd );
			result.append("\ndecrypted passwd = " + getDecryptedPasswd() );
			
		} catch (Exception e) {
			log.error("", e);
			
		} 
		
		return result.toString();
	}
}
