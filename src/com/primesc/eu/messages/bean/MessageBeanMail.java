package com.primesc.eu.messages.bean;

public class MessageBeanMail extends MessageBean {

	private String	subject;
	private String	attachements;
	private String[]	attachementsArray;
	
	private String	fromName;
	private String  toName;

	public MessageBeanMail() {
		super();
		setCost( 0 );
		setPriority("3");
	}
	
	public MessageBeanMail(MessageBean bean) {
		super();
		setCost( 0 );
		setNewBean(bean);
	}
	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the attachements
	 */
	public String[] getAttachementsArray() {
		if( attachementsArray==null && attachements!=null && attachements.length()>0 )
			attachementsArray = attachements.split(";");
		
		else if( attachementsArray==null )
			attachementsArray = new String[]{};
		
		return attachementsArray;
	}
	/**
	 * @return the attachements
	 */
	public String getAttachements() {
		return attachements;
	}
	/**
	 * @param attachements the attachements to set
	 */
	public void setAttachements(String attachements) {
		this.attachements = attachements;
		this.attachementsArray=null;
	}

	/**
	 * @return the fromName
	 */
	public String getFromName() {
		return fromName;
	}

	/**
	 * @param fromName the fromName to set
	 */
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	/**
	 * @return the toName
	 */
	public String getToName() {
		return toName;
	}

	/**
	 * @param toName the toName to set
	 */
	public void setToName(String toName) {
		this.toName = toName;
	}
}
