package com.primesc.eu.messages.bean;

import com.primesc.eu.bean.bulkness.MTMessageResponse;
import com.primesc.eu.values.SendMessages.Status;

public class MessageBeanSMS extends MessageBean {
	private String	country;
	private int		content_parts;
	
	public MessageBeanSMS() {
		super();
	}
	
	public MessageBeanSMS(MessageBean bean) {
		super();
		setCost( 0 );
		setNewBean(bean);
	}

	public void setResponse(MTMessageResponse response){
		MTMessageResponse.Reply reply = response.getReply().get(0);
		
		this.country = reply.getCountry();
		this.content_parts = ( reply.getParts()>0) ? reply.getParts() : this.content_parts ;
		
		try{
			this.cost = Double.valueOf( reply.getCost() );
			
		} catch(Exception e) {
			this.cost = 0.0;
		}
		
		this.msg_id = reply.getMessage_id()!=null ? reply.getMessage_id() : this.msg_id;
		this.status = reply.getStatus().equalsIgnoreCase("OK") ? Status.SENT_OK.getStatus() : Status.FAILED.getStatus();
		this.reply  = response.getReplyText();
	}
	
	/**
	 * @return the country
	 */
	@Override
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	@Override
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the content_parts
	 */
	public int getContent_parts() {
		return content_parts;
	}

	/**
	 * @param content_parts the content_parts to set
	 */
	public void setContent_parts(int content_parts) {
		this.content_parts = content_parts;
	}

}
