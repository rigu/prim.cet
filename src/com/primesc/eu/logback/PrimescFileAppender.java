package com.primesc.eu.logback;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;

import com.primesc.eu.values.PrimescEu;

public class PrimescFileAppender extends RollingFileAppender<ILoggingEvent> {
	
	private String folderName 	= null;
	private String fileName		= null;
	
	
	/* (non-Javadoc)
	 * @see ch.qos.logback.core.FileAppender#setFile(java.lang.String)
	 */
	@Override
	public void setFile(String file) {
		if( PrimescEu._forTest ) {
			setFileFolderNames(file);		
					
			if( folderName!=null && folderName.length()>0 
					&& fileName!=null && fileName.length()>0 ) 
			{
				String realPath = PrimescFileAppender.class.getResource("/").getPath() + "../../";
				
				if( realPath!=null && realPath.indexOf('/')==0 )
					realPath = realPath.substring(1);
				
				file = realPath + folderName + "/" + fileName;
			}
		} else {
			//file = _Constants._LOGGER_FOLDER ;
		}
		System.out.println(">>>" + file);
		super.setFile(file);
	}
	
	private void setFileFolderNames( String str ) {
		String[] fileFolder = null;
		
		if ( str==null || str.length()==0 ) {
			fileFolder = new String[]{};
			
		} else{
			fileFolder = str.split("/");
		}
		
		if( fileFolder.length > 2 )
			return;
		
		switch( fileFolder.length ) {
			case 1 :
				folderName 	= fileName = fileFolder[0];
				break;
				
			case 2 :
				folderName 	= fileFolder[0];
				fileName 	= fileFolder[1];
				break;
				
			default :
				folderName = fileName = "primesc_eu_log";
		}	
	}
}
