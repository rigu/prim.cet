/**
 * 
 */
package com.primesc.eu.logback;

import java.io.InputStream;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import com.primesc.eu.values.PrimescEu;

/**
 * 
 * @author Igor Afteni
 *
 */
public class LogbackConfigLoader {
	
	public static String loadLogBackConfig( PrimescEu.LoggerValues val )
	{
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

		InputStream logbackFile = LogbackConfigLoader.class.getResourceAsStream( val.toString() );
				
		JoranConfigurator config = new JoranConfigurator();
		config.setContext( loggerContext );
		
		loggerContext.reset();
		try {
			config.doConfigure(logbackFile);
			
		} catch (JoranException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);			
		}
		
		//logger.info("Logger Context = " + loggerContext.getName() );
		//logger.info ("Configured Logback with config file from: " + logbackFile);
		
		return val.toString();
	}
}
