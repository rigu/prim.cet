package com.primesc.eu.logback;

import ch.qos.logback.core.PropertyDefinerBase;

import com.primesc.eu.values.PrimescEu;


public class PrimescEuDefiner extends PropertyDefinerBase {

	private String mydef = "";
	
	@Override
	public String getPropertyValue() {
		//System.out.println("get property MYDEF = " + this.mydef);
		return mydef;
	}

	/**
	 * @return the mydef
	 */
	public synchronized String getMydef() {
		//System.out.println("get MYDEF = " + this.mydef);
		return mydef;
	}

	/**
	 * @param myd the mydef to set
	 */
	public synchronized void setMydef(String myd) {
		this.mydef = PrimescEu.LoggerValues.FOLDER.toString();
		//System.out.println("MYDEF = " + this.mydef);
	}	
	
	
}
