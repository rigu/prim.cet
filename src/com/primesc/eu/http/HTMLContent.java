package com.primesc.eu.http;

import java.io.IOException;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.primesc.eu.exceptions.HTMLException;
import com.primesc.eu.values.PrimescEu;

public class HTMLContent {
	private static final Logger log = LoggerFactory.getLogger( HTMLContent.class );
	
	private String url=null;
	
	public HTMLContent(){}
	
	public HTMLContent( String url ) throws HTMLException {
		setUrl( url );
	}
	
	public Document getDocument( String url ) throws IOException, HTMLException {
		setUrl(url);
		
		return getDocument();
	}
	
	public Document getDocument() throws IOException, HTMLException {
		Connection connection = getConnection();
		Response response = connection.execute();
		
		Document doc = null;
		
		int statusCode = response.statusCode();	    
	    if( statusCode == 200 ) {
	    	doc = connection.get();
	    	
	    } else {
	    	throw new HTMLException( "Response status code = " + statusCode 
	    									+ "Cannot get HTML Document from " + url );
	    }
	    
	    return doc;
	}
	
	public Elements getElements( String url, String element ) throws IOException, HTMLException{
		Document doc = getDocument( url );		
		return doc.select(element);
	}
	
	public Elements getElements( String element ) throws IOException, HTMLException{
		Document doc = getDocument();		
		return doc.select(element);
	}
	
	public Connection getConnection() throws HTMLException {
		log.info("getConnection for URL: " + url);
		
		if( url==null || url.length()==0 )
			throw new HTMLException("URL string cannot be null or vid");
		
		if( PrimescEu._forTest ){
			log.info(" set system proxy ");
			/**************************************************/
		}
		
		log.info("get JSoup");
		
		Connection conn = Jsoup.connect( url )
				.data("query", "HTML")
				//.ignoreHttpErrors(true).method(Method.GET)
				.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1").timeout(60*1000);
		
		return conn;
	}

	/**
	 * @return the url
	 */
	public synchronized String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 * @throws HTMLException 
	 */
	public synchronized void setUrl(String url) throws HTMLException {
		if( url==null || url.length()==0 )
			throw new HTMLException("URL string cannot be null or vid");
		
		this.url = url;
	}
	
	
}
