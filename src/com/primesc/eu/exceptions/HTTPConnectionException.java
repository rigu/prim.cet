/**
 * 
 */
package com.primesc.eu.exceptions;

/**
 * @author Igor Afteni
 *
 */
public class HTTPConnectionException extends Exception {
	private static final long serialVersionUID = 4391695312147587940L;

	private String httpConnectionMessage;

	public HTTPConnectionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HTTPConnectionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		setHttpConnectionMessage(arg0);
	}

	public HTTPConnectionException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		setHttpConnectionMessage(arg0);
	}

	public HTTPConnectionException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the httpConnectionMessage
	 */
	public synchronized String getHttpConnectionMessage() {
		return httpConnectionMessage;
	}

	/**
	 * @param httpConnectionMessage the httpConnectionMessage to set
	 */
	public synchronized void setHttpConnectionMessage(String httpConnectionMessage) {
		this.httpConnectionMessage = httpConnectionMessage;
	}
}
