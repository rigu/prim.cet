/**
 * 
 */
package com.primesc.eu.exceptions;

import com.primesc.eu.messages.MessageAlert;

/**
 * @author Igor Afteni
 *
 */
public abstract class CriticalException extends Exception {
	private static final long serialVersionUID = 8579638328738170749L;

	protected String criticalMessage;
	
	public CriticalException() {
		setCriticalMessage( "Primesc eu critical exception" );
	}

	/**
	 * @param message
	 */
	public CriticalException(String message) {
		super(message);
		setCriticalMessage( message );
		
	}

	/**
	 * @param cause
	 */
	public CriticalException(Throwable cause) {
		super(cause);
		setCriticalMessage( cause.getMessage() );
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CriticalException(String message, Throwable cause) {
		super(message, cause);
		setCriticalMessage( message );
	}

	/**
	 * @return the criticalMessage
	 */
	public synchronized String getCriticalMessage() {
		return criticalMessage;
	}

	/**
	 * @param criticalMessage the criticalMessage to set
	 */
	public synchronized void setCriticalMessage(String criticalMessage) {
		this.criticalMessage = criticalMessage;
		( new MessageAlert() ).sendError( criticalMessage ); 
	}

	
}
