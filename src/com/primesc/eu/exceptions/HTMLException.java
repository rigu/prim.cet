/**
 * 
 */
package com.primesc.eu.exceptions;

import com.primesc.eu.messages.MessageAlert;

/**
 * @author Igor Afteni
 *
 */
public class HTMLException extends Exception {
	private static final long serialVersionUID = 1377485476134623710L;

	private String htmlMessage;

	public HTMLException() {
		super("HTML Exception");
		setHtmlMessage( "HTML Exception" );
	}

	public HTMLException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		setHtmlMessage(arg0);
	}

	public HTMLException(String arg0) {
		super(arg0);
		setHtmlMessage(arg0);
	}

	public HTMLException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the htmlMessage
	 */
	public synchronized String getHtmlMessage() {
		return htmlMessage;
	}

	/**
	 * @param htmlMessage the htmlMessage to set
	 */
	public synchronized void setHtmlMessage(String htmlMessage) {
		this.htmlMessage = htmlMessage;
		( new MessageAlert() ).sendWarn( htmlMessage );
	}
	
	
}
