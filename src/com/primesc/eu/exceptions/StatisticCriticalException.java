/**
 * 
 */
package com.primesc.eu.exceptions;

/**
 * @author Igor Afteni
 *
 */
public class StatisticCriticalException extends CriticalException {
	private static final long serialVersionUID = -463100422868952840L;

	/**
	 * 
	 */
	public StatisticCriticalException() {
		super("Statistic Critical Exception");		
	}

	/**
	 * @param message
	 */
	public StatisticCriticalException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public StatisticCriticalException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public StatisticCriticalException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
