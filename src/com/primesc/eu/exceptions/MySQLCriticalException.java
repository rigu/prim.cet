/**
 * 
 */
package com.primesc.eu.exceptions;

/**
 * @author Igor Afteni
 *
 */
public class MySQLCriticalException extends CriticalException {
	private static final long serialVersionUID = 7050374143578275446L;
		
	public MySQLCriticalException() {
		setCriticalMessage("MySQL Critical Exception");
	}

	/**
	 * @param message
	 */
	public MySQLCriticalException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public MySQLCriticalException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public MySQLCriticalException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
